const { assert } = require('chai');
const { newUser } = require('../index.js');

//Test kit
describe('Test newUser object', () => {
    //Handle the test case
    it('Assert newUser type is an object', () => {
        assert.equal(typeof(newUser), 'object')
    });

    //Mini-Activity
    it('Assert newUser firstName is type string', () => {
        assert.equal(typeof(newUser.firstName), 'string')
    });

    it('Assert that newUser firstName is not undefined',() => {
        assert.notEqual(typeof(newUser.firstName), 'undefined')
    });

    it('Assert newUser lastName is type string', () => {
        assert.equal(typeof(newUser.lastName), 'string')
    });

    it('Assert that newUser lastName is not undefined',() => {
        assert.notEqual(typeof(newUser.lastName), 'undefined')
    });

    it('Assert newUser age has at least 16 characters', () => {
        assert.isAtLeast(newUser.age, 18)
    });

    it('Assert newUser age is type number', () => {
        assert.equal(typeof(newUser.age), 'number')
    });

    it('Assert newUser contact is type number', () => {
        assert.equal(typeof(newUser.contactNo), 'string')
    });

    it('Assert newUser batchNo is type number', () => {
        assert.equal(typeof(newUser.batchNo), 'number')
    });

    it('Assert that newUser batchNo is not undefined',() => {
        assert.notEqual(typeof(newUser.batchNo), 'undefined')
    });
    //End of Mini-Activity

    it('Assert newUser email is type string', () => {
        assert.equal(typeof(newUser.email), 'string')
    });

    it('Assert that newUser email is not undefined',() => {
        assert.notEqual(typeof(newUser.email), 'undefined')
    });

    it('Assert newUser password is type string', () => {
        assert.equal(typeof(newUser.password), 'string')
    });

    it('Assert newUser password has at least 16 characters', () => {
        assert.isAtLeast(newUser.password.length, 16)
    })
});

