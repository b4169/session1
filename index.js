const express = require('express')
const app = express()
const port = 5000

app.use(express.json())

//Object
const newUser = {
    firstName : 'levi',
    lastName  : 'ackerman',
    age       : 28,
    contactNo : '09123456789',
    batchNo   : 166,
    email     : 'leviAckerman@mail.com',
    password  : 'thequickbrownfoxjumpsoverthelazydog'
};

module.exports = {
    newUser : newUser
}

app.listen(port, () => {
  console.log(`Port is running ${port}`)
})